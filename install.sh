#!/bin/bash 

if [[ $(lsb_release -i) = *ManjaroLinux* ]]
then
    echo "You are using ManjaroLinux."
    echo "Installing fish"
    pacman -S --noconfirm fish
fi
