#!/bin/bash

# Execute on interactive shell
[[ $- != *i* ]] && return

use_color=true

if ${use_color} ; then
    alias ls='ls --color=auto'
    alias grep='grep --colour=auto'
    alias egrep='egrep --colour=auto'
	alias fgrep='fgrep --colour=auto'
	
	if [[ ${EUID} == 0 ]] ; then
		PS1='\[\033[01;31m\][\u@\h\[\033[01;37m\] \w\[\033[01;31m\]]\$\[\033[00m\] ' #For root shell
	else
		PS1='\[\033[01;32m\][\u@\h\[\033[01;37m\] \w\[\033[01;32m\]]\$\[\033[00m\] ' #For non-root shell
	fi
else
	if [[ ${EUID} == 0 ]] ; then
		PS1='\u@\h \W \$ ' #For root shell
	else
		PS1='\u@\h \w \$ ' #For non-root shell
	fi
fi

unset use_color

alias ll='ls -la'
alias df='df -h'                          # human-readable sizes
alias du='du -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB

# Confirm unsafe file operations.
alias cp='/bin/cp -i'
alias mv='/bin/mv -i'
#alias rm='/bin/rm -i'

# When invoked without arguments g will do a short Git status, otherwise it will just pass on the given arguments to the git command. Status is likely to the be Git command one will execute the most, hence this simple enhancement does prove very useful in practice.
alias g='_f() { if [[ $# == 0 ]]; then git status --short --branch; else git "$@"; fi }; _f'

# Bash won't get SIGWINCH if another process is in the foreground.
# Enable checkwinsize so that bash will check the terminal size when
# it regains control.  #65623
# http://cnswww.cns.cwru.edu/~chet/bash/FAQ (E11)
shopt -s checkwinsize

# When set, aliases are usable when shell is non interactive
shopt -s expand_aliases

# Better History
# https://mrzool.cc/writing/sensible-bash/

# Append to the history file, don't overwrite it
shopt -s histappend

# Save multi-line commands as one command
shopt -s cmdhist

# Huge history. Doesn't appear to slow things down, so why not?
HISTSIZE=500000
HISTFILESIZE=100000

# Avoid duplicate entries
HISTCONTROL="erasedups:ignoreboth"

# Don't record some commands
export HISTIGNORE="&:[ ]*:exit:ls:bg:fg:history"

# Useful timestamp format
HISTTIMEFORMAT='%F %T '

# Better, faster directory navigation I
# https://mrzool.cc/writing/sensible-bash/

shopt -s autocd
shopt -s dirspell
shopt -s cdspell

# Extractor function - default in Manjaro :)
# # ex - archive extractor
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}
